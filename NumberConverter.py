def Strip(user_number):
    user_number = str(user_number)
    user_number = user_number.lstrip("0")
    user_number = user_number.replace(",","")
    return user_number

def decimal(dec):
    decVal =""
    x=0
    digits = NumberOfDigits(dec)
    dec = str(dec)
    while x<digits:
       decVal += NumberList(int(dec[x]))
       x+=1
    return decVal

def NepaliDecimal(dec):
    decVal =""
    x=0
    digits = NumberOfDigits(dec)
    dec = str(dec)
    while x<digits:
       decVal += NepaliNumberList1(int(dec[x]))
       x+=1
    return decVal

def NumberOfDigits(user_number):
    """"counts the number of digits 
         parameters :
         arg1 (int): usernumber
         return number of digits """
    proxy = user_number
    Count = 0
    while proxy > 0:
        proxy = proxy // 10
        Count = Count + 1
    return Count

def NumberList(digit):
    """ List for numberes one to nine
         parameters :
         arg1 (int): usernumber
         return """
    value = ""     
    if digit == 0:
        value+="ZERO"
    elif digit == 1:
        value +="ONE "
    elif digit == 2:
        value +="TWO "
    elif digit == 3:
        value +="THREE "
    elif digit == 4:
        value +="FOUR "
    elif digit == 5:
        value +="FIVE "
    elif digit == 6:
        value +="SIX "
    elif digit == 7:
        value +="SEVEN "
    elif digit == 8:
        value +="EIGHT "
    elif digit == 9:
        value +="NINE "
    else:
        value +=""
    return value 

def NumberList2(digit):
    """ List for numberes Eleven to nineteen
         parameters :
         arg1 (int): usernumber
         return """
    value = ""
    if digit == 0:
        value+="TEN "
    elif digit == 1:
        value+="ELEVEN " 
    elif digit == 2:
        value+="TWELVE " 
    elif digit == 3:
        value+="THIRTEEN "
    elif digit == 4:
        value+="FOURTEEN "
    elif digit == 5:
        value+="FIVTEEN "
    elif digit == 6:
        value+="SIXTEEN "
    elif digit == 7:
        value+="SEVENTEEN "
    elif digit == 8:
        value+="EIGHTEEN "
    else:
        value+="NINETEEN "
    return value 

def NumberList3(digit):
    """"List for numberes twenty to ninety  
         parameters :
         arg1 (int): usernumber
         return number of digits """
    val=""
    if digit == 1:
        val+="TEN "
    elif digit == 2:
       val +="TWENTY "
    elif digit == 3:
       val +="THIRTY "
    elif digit == 4:
       val +="FOURTY "
    elif digit == 5:
       val +="FIFTY "
    elif digit == 6:
       val +="SIXTY "
    elif digit == 7:
       val +="SEVENTY "
    elif digit == 8:
       val +="EIGHTY "
    else:
       val +="NINETY "
    return val 

def TwoDigit(user_number):
    """"Gets a two digit number into words 
         parameters :
         arg1 (int): usernumber    """
    val = "" 
    if user_number < 20:
        return NumberList2(user_number % 10)
    else:
        val+= NumberList3(user_number // 10)
        val+= NumberList(user_number % 10)
    return val 

def NumberToWord(user_number):
    """"Takes in numerical value returns it in words
         parameters :
         arg1 (int): usernumber    """
    global value
    decVal=""
    if "," in str(user_number):
        user_number = Strip(user_number)
    if "."  in str(user_number):
        split = str(user_number).split(".")
        user_number = int(split[0])
        decVal = "POINT "
        decVal += decimal(int(split[1]))
    digits = NumberOfDigits(int(user_number))
    user_number = int(user_number)
    if digits == 1:
        if user_number == 0:
            value += "Zero"
        else:
            value += NumberList(int(user_number))           
    elif digits == 2:
        value += TwoDigit(user_number)
    elif digits == 3:
        value += NumberList(user_number // 100)
        value += "HUNDRED "
        
        NumberToWord(user_number % 100)
    elif digits == 4:
        value += NumberList(user_number // 1000)
        value += "THOUSAND "
        NumberToWord(user_number % 1000)
    elif digits == 5:
        value += TwoDigit(user_number // 1000)
        value += "THOUSAND "
        NumberToWord(user_number % 1000)
    elif digits == 6:
        value += NumberList(user_number // 100000)
        value += "LAKH "
        NumberToWord(user_number % 100000)
    elif digits == 7:
        value += TwoDigit(user_number // 100000)
        value += "LAKH "
        NumberToWord(user_number % 100000)
    elif digits == 8:
        value += NumberList(user_number // 10000000)
        value += "CRORE "
        NumberToWord(user_number % 10000000)
    elif digits == 9:
        value += TwoDigit(user_number // 10000000)
        value += "CRORE "
        NumberToWord(user_number % 10000000)
    elif digits == 10:
        value += NumberList(user_number // 1000000000)
        value += "ARAB "
        NumberToWord(user_number % 1000000000)
    elif digits == 11:
        value += TwoDigit(user_number // 1000000000)
        value += "ARAB "
        NumberToWord(user_number % 1000000000)
    elif digits == 12:
        value += NumberList(user_number // 100000000000)
        value += "KHARAB "
        NumberToWord(user_number % 100000000000)
    elif digits == 13:
        value += TwoDigit(user_number // 100000000000)
        value += "KHARAB "
        NumberToWord(user_number % 100000000000)
    else:
        print("")
    return(value + decVal)

def NepaliNumberList1(user_number):
    """"for numberes one to nine 
         parameters :
         arg1 (int): usernumber    """
    NumList = ["शुन्य ", "एक ", "दुई ", "तीन ", "चार ", "पाँच ", "छ ", "सात ", "आठ ", "नौ ", "दश "]
    req = NumList[user_number]
    return req

def NepaliNumberList2(user_number):
    """"ist for numberes 0 to 99
         parameters :
         arg1 (int): usernumber    """
    NumLists = [
        "शुन्य ",
        "एक ",
        "दुई ",
        "तीन ",
        "चार ",
        "पाँच ",
        "छ ",
        "सात ",
        "आठ ",
        "नौ ",
        "दश ",
        "एघार ",
        "बाह्र ",
        "तेह्र ",
        "चौध ",
        "पन्ध्र ",
        "सोह्र ",
        "सत्र ",
        "अठार ",
        "उन्नाइस ",
        "विस ",
        "एक्काइस ",
        "बाइस ",
        "तेईस ",
        "चौविस ",
        "पच्चिस ",
        "छब्बिस ",
        "सत्ताइस ",
        "अठ्ठाईस ",
        "उनन्तिस ",
        "तिस ",
        "एकत्तिस ",
        "बत्तिस ",
        "तेत्तिस ",
        "चौँतिस ",
        "पैँतिस ",
        "छत्तिस ",
        "सैँतीस ",
        "अठतीस ",
        "उनन्चालीस ",
        "चालीस ",
        "एकचालीस ",
        "बयालीस ",
        "त्रियालीस ",
        "चवालीस ",
        "पैँतालीस ",
        "छयालीस ",
        "सच्चालीस ",
        "अठचालीस ",
        "उनन्चास ",
        "पचास ",
        "एकाउन्न ",
        "बाउन्न ",
        "त्रिपन्न ",
        "चउन्न ",
        "पचपन्न ",
        "छपन्न ",
        "सन्ताउन्न ",
        "अन्ठाउन्न ",
        "उनन्साठी ",
        "साठी ",
        "एकसट्ठी ",
        "बयसट्ठी ",
        "त्रिसट्ठी ",
        "चौंसट्ठी ",
        "पैंसट्ठी ",
        "छयसट्ठी ",
        "सतसट्ठी ",
        "अठसट्ठी ",
        "उनन्सत्तरी ",
        "सत्तरी ",
        "एकहत्तर ",
        "बहत्तर ",
        "त्रिहत्तर ",
        "चौहत्तर ",
        "पचहत्तर ",
        "छयहत्तर ",
        "सतहत्तर ",
        "अठहत्तर ",
        "उनासी ",
        "असी ",
        "एकासी ",
        "बयासी ",
        "त्रियासी ",
        "चौरासी ",
        "पचासी ",
        "छयासी ",
        "सतासी ",
        "अठासी ",
        "उनान्नब्बे ",
        "नब्बे ",
        "एकान्नब्बे ",
        "बयानब्बे ",
        "त्रियान्नब्बे ",
        "चौरान्नब्बे ",
        "पन्चानब्बे ",
        "छयान्नब्बे ",
        "सन्तान्नब्बे ",
        "अन्ठान्नब्बे ",
        "उनान्सय ",
    ]
    req = NumLists[user_number]
    return req

def NepaliNumberToWord(user_number):
    """"Takes in numerical value returns it in words
         parameters :
         arg1 (int): usernumber    """
    global NepaliValue 
    decVal=""
    if "," in str(user_number):
        user_number = Strip(user_number)
    if "."  in str(user_number):
        split = str(user_number).split(".")
        user_number = int(split[0])
        decVal = "दशमलव "
        decVal += NepaliDecimal(int(split[1]))
    user_number = int(user_number)
    digits = NumberOfDigits(user_number)
    if digits == 1:
        if user_number == 0:
           NepaliValue += "शुन्य "
        else:
           NepaliValue +=  NepaliNumberList1(user_number)
    elif digits == 2:
        NepaliValue += NepaliNumberList2(user_number)
    elif digits == 3:
        NepaliValue += NepaliNumberList2(user_number // 100)
        NepaliValue += "सय "
        NepaliNumberToWord(user_number % 100) 
    elif digits == 4:
        NepaliValue += NepaliNumberList2(user_number // 1000)
        NepaliValue += "हजार "
        NepaliNumberToWord(user_number % 1000)
    elif digits == 5:
        NepaliValue += NepaliNumberList2(user_number // 1000)
        NepaliValue += "हजार "
        NepaliNumberToWord(user_number % 1000)
    elif digits == 6:
        NepaliValue += NepaliNumberList2(user_number // 100000)
        NepaliValue += "लाख "
        NepaliNumberToWord(user_number % 100000)
    elif digits == 7:
        NepaliValue += NepaliNumberList2(user_number // 100000)
        NepaliValue += "लाख "
        NepaliNumberToWord(user_number % 100000)
    elif digits == 8:
        NepaliValue += NepaliNumberList2(user_number // 10000000)
        NepaliValue += "करोड़ "
        NepaliNumberToWord(user_number % 10000000)
    elif digits == 9:
        NepaliValue += NepaliNumberList2(user_number // 10000000)
        NepaliValue += "करोड़ "
        NepaliNumberToWord(user_number % 10000000)
    elif digits == 10:
        NepaliValue += NepaliNumberList2(user_number // 1000000000)
        NepaliValue += "अर्ब "
        NepaliNumberToWord(user_number % 1000000000)
    elif digits == 11:
        NepaliValue += NepaliNumberList2(user_number // 1000000000)
        NepaliValue += "अर्ब "
        NepaliNumberToWord(user_number % 1000000000)
    elif digits == 12:
        NepaliValue += NepaliNumberList2(user_number // 100000000000)
        NepaliValue += "खर्ब "
        NepaliNumberToWord(user_number % 100000000000)
    elif digits == 13:
        NepaliValue += NepaliNumberList2(user_number // 100000000000)
        NepaliValue += "खर्ब "
        NepaliNumberToWord(user_number % 100000000000)
    else:
        print("")
    return (NepaliValue + decVal)

def FirstDigit(splitted):
    """"Prints the First Digit
         parameters :
         arg1 (int): splitted    """
    count = 0
    i = 0
    LIST = [
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
        "ten",
        "eleven",
        "twelve",
        "thirteen",
        "fourteen",
        "fifteen",
        "sixteen",
        "seventeen",
        "eighteen",
        "nineteen",
    ]
    while i < len(LIST):
        count += 1
        if LIST[i] == splitted:
            print(count, end="")
        i += 1

def SecondPart(splitted):
    """"Prints the Second Digit
         parameters :
         arg1 (int): splitted    """
    count = 0
    i = 0
    LIST = [
        "ten",
        "twenty",
        "thirty",
        "fourty",
        "fifty",
        "sixty",
        "seventy",
        "eighty",
        "ninety",
    ]
    while i < len(LIST):
        count += 1
        if LIST[i] == splitted:
            print(count, end="")
        i += 1

def Splitter(user_number):
    """"Splits the using whitespaces
         parameters :
         arg1 (int): user_number    
         Return (str) : Splitted"""
    string = str(user_number)
    splitted = string.split()
    return splitted

def WordToNumber(user_number):
    """"Prints out Digits for the words
         parameters :
         arg1 (int): user_number    """
    splitted = Splitter(user_number)
    if len(splitted) == 1:

        if (
            (splitted[0] != "twenty")
            and (splitted[0] != "thirty")
            and (splitted[0] != "fourty")
            and (splitted[0] != "fifty")
            and (splitted[0] != "sixty")
            and (splitted[0] != "fourty")
            and (splitted[0] != "seventy")
            and (splitted[0] != "eighty")
            and (splitted[0] != "ninety")
        ):

            FirstDigit(splitted[0])
        else:
            SecondPart(splitted[0])
            print(0, end="")
    elif len(splitted) == 2:
        if (
            (splitted[0] != "one")
            and (splitted[0] != "two")
            and (splitted[0] != "three")
            and (splitted[0] != "four")
            and (splitted[0] != "five")
            and (splitted[0] != "six")
            and (splitted[0] != "seven")
            and (splitted[0] != "eight")
            and (splitted[0] != "nine")
        ):
            SecondPart(splitted[0])
            FirstDigit(splitted[1])
        else:
            FirstDigit(splitted[0])
            print("00", end="")

    elif len(splitted) == 3:
        FirstDigit(splitted[0])
        if (
            (splitted[2] != "twenty")
            and (splitted[2] != "thirty")
            and (splitted[2] != "fourty")
            and (splitted[2] != "fifty")
            and (splitted[2] != "sixty")
            and (splitted[2] != "fourty")
            and (splitted[2] != "seventy")
            and (splitted[2] != "eighty")
            and (splitted[2] != "ninety")
        ):
            if len(splitted[2]) < 6:
                print(0, end="")
            FirstDigit(splitted[2])
        else:
            SecondPart(splitted[2])
            print(0, end="")

    elif len(splitted) == 4:
        FirstDigit(splitted[0])
        SecondPart(splitted[2])
        FirstDigit(splitted[3])
    else:
        print("")

def Day(day):
    """"Prints thr day
         parameters :
         arg1 (int): Day    
    """
    day = int(day)
    if day > 31:
        print("Wrong Date")
    else:
        days = [
            "शुन्य ",
        "एक ",
        "दुई ",
        "तीन ",
        "चार ",
        "पाँच ",
        "छ ",
        "सात ",
        "आठ ",
        "नौ ",
        "दश ",
        "एघार ",
        "बाह्र ",
        "तेह्र ",
        "चौध ",
        "पन्ध्र ",
        "सोह्र ",
        "सत्र ",
        "अठार ",
        "उन्नाइस ",
        "विस ",
        "एक्काइस ",
        "बाइस ",
        "तेईस ",
        "चौविस ",
        "पच्चिस ",
        "छब्बिस ",
        "सत्ताइस ",
        "अठ्ठाईस ",
        "उनन्तिस ",
        "तिस ",
        "एकत्तिस ",
        ]
    return(days[day])

def Month(month):
    """"Prints the month
         parameters :
         arg1 (int): Month    
     """
    month = int(month)
    if month > 12:
        print("Wrong Date")
    else:
        months = [
            "January ",
            "Febuary ",
            "March ",
            "April ",
            "May ",
            "June ",
            "July ",
            "August ",
            "Spetember ",
            "October ",
            "November ",
            "Decemeber ",
        ]
        return(months[month - 1])
def NepMonth(month):
    """"Prints the month
         parameters :
         arg1 (int): Month    
     """
    month = int(month)
    if month > 12:
        print("Wrong Date")
    else:
        months = [ "जनवरी ", "फेब्रुअरी ", "मार्च ", "अप्रिल ", "सक्छ" ,"जून ", "जुलाई " ,"अगस्त ",
                 "सेप्टेम्बर ","अक्टुबर ","नोभेम्बर ","डिसेम्बर"  ]
        return(months[month - 1])
def NepaliMonth(month):
     """"Prints the month
         parameters :
         arg1 (int): Month    
     """
     month = int(month)
     if month > 12:
        print("Wrong Date")
     else:
        months = [ "पुष ", "माघ ", "फाल्गुन ", "चैत्र " ,"बैशाख ", "जेठ " ,"असार ",
                 "श्रावण ","भदौ ","आश्विन ","कार्तिक","मंसिर"  ]
        return(months[month - 1])
def DateConverter(inp):
    store =""
    inp = int(inp)
    FirstTwo = str(inp // 1000000)
    FirstTwo = FirstTwo.strip("0")
    store += Day(FirstTwo)
    Third = inp // 10000
    SecondTwo = str(Third % 100)
    SecondTwo = SecondTwo.strip("0")
    store += Month(SecondTwo)
    #CALL NepaliMonth for nepali month
    #CALL NepMonth if you want english months in Nepali 
    LastFour = inp % 10000
    store +=NepaliNumberToWord(LastFour)
    return store
def years(inp):
    inp=int(inp)
    year=""
    year += NepaliNumberToWord(inp//100)
    year = NepaliNumberToWord(inp%100)
    lis = year.split()
    lis.insert(1,"सय")
    str1 = ' '.join(lis)
    return str1
value = ""
"""Global var used in NumberToWord"""
NepaliValue = ""
"""Global var used in NepaliNumberToWord"""
var = input("N for number or W for word or D for date" + "\ns")
if (var == "N") or (var == "n"):
    user_number = input("Enter your number between one to thirteen digit" + "\n")
    ans = input("choose N for nepali or E for english" + "\n")
    if (ans == "N") or (ans == "n"): 
        print(NepaliNumberToWord(user_number))
    else:
        print(NumberToWord(user_number))
      
elif (var == "W") or (var == "w"):
    user_number = input("Enter your word" + "\n")
    if isinstance(user_number, str):
        WordToNumber(user_number)
elif (var == "D") or (var == "d"):
    inp = input("Enter the 8 digits of date" + "\n")
    inp = inp.replace("/","")
    inp = inp.replace("-","")

    if len(inp) != 8:
        print("Wrong Input")
    else:
        print(DateConverter(inp))
else:
    inp = input("desired year"+"\n")
    if len(inp) != 4:
        print("Wrong Input")
    else:
        year = years(inp)
        print(year)
    